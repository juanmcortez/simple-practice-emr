<?php

namespace App\Filament\Pages\Auth;

use Filament\Forms\Form;
use Filament\Facades\Filament;
use App\Models\Commons\Profile;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Hidden;
use Filament\Forms\Components\TextInput;
use Filament\Notifications\Notification;
use Filament\Forms\Components\DatePicker;
use Filament\Pages\Auth\Register as BaseRegister;
use Filament\Http\Responses\Auth\Contracts\RegistrationResponse;
use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;


class Register extends BaseRegister
{
    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Group::make()
                    ->columns(2)
                    ->schema([
                        TextInput::make('email')
                            ->prefixIcon('heroicon-o-envelope')
                            ->autofocus()
                            ->email()
                            ->columnSpanFull(),

                        TextInput::make('username')
                            ->prefixIcon('heroicon-o-user')
                            ->maxLength(128)
                            ->columnSpanFull()
                            ->required(),

                        Hidden::make('profile_id'),

                        $this->getPasswordFormComponent()
                            ->prefixIcon('heroicon-o-lock-closed'),
                        $this->getPasswordConfirmationFormComponent()
                            ->prefixIcon('heroicon-o-lock-closed'),
                    ]),

                Group::make()
                    ->columns(2)
                    ->schema([
                        TextInput::make('profile.last_name')
                            ->required()
                            ->maxLength(64),
                        TextInput::make('profile.first_name')
                            ->required()
                            ->maxLength(64),
                        TextInput::make('profile.middle_name')
                            ->maxLength(32),

                        DatePicker::make('profile.date_of_birth')
                            ->label('Birthdate')
                            ->prefixIcon('heroicon-o-calendar-days')
                            ->displayFormat('M d, Y')
                            ->native(false)
                            ->weekStartsOnSunday()
                            ->minDate(now()->subYears(100))
                            ->maxDate(now())
                            ->closeOnDateSelection()
                            ->required(),
                    ]),
            ]);
    }


    public function register(): ?RegistrationResponse
    {
        try {
            $this->rateLimit(2);
        } catch (TooManyRequestsException $exception) {
            Notification::make()
                ->title(__('filament-panels::pages/auth/register.notifications.throttled.title', [
                    'seconds' => $exception->secondsUntilAvailable,
                    'minutes' => ceil($exception->secondsUntilAvailable / 60),
                ]))
                ->body(array_key_exists('body', __('filament-panels::pages/auth/register.notifications.throttled') ?: []) ? __('filament-panels::pages/auth/register.notifications.throttled.body', [
                    'seconds' => $exception->secondsUntilAvailable,
                    'minutes' => ceil($exception->secondsUntilAvailable / 60),
                ]) : null)
                ->danger()
                ->send();

            return null;
        }

        $data = $this->form->getState();

        // Store the profile before creating the user.
        $profile = Profile::factory()->createOne([
            "last_name"     => $data['profile']['last_name'],
            "first_name"    => $data['profile']['first_name'],
            "middle_name"   => $data['profile']['middle_name'],
            "date_of_birth" => $data['profile']['date_of_birth'],
        ]);
        $data['profile_id'] = $profile->id;
        // Store the profile before creating the user.

        $user = $this->getUserModel()::create($data);

        $this->sendEmailVerificationNotification($user);

        Filament::auth()->login($user);

        session()->regenerate();

        return app(RegistrationResponse::class);
    }
}
