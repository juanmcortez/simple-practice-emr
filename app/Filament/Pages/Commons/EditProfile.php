<?php

namespace App\Filament\Pages\Commons;

use Filament\Forms\Get;
use Filament\Forms\Form;
use App\Models\Settings\ListBuilder;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\DatePicker;
use Filament\Pages\Auth\EditProfile as BaseEditProfile;

class EditProfile extends BaseEditProfile
{

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Group::make()
                    ->relationship('profile')
                    ->columns(2)
                    ->schema([

                        TextInput::make('last_name')
                            ->required()
                            ->autofocus()
                            ->maxLength(64),
                        TextInput::make('first_name')
                            ->required()
                            ->maxLength(64),
                        TextInput::make('middle_name')
                            ->maxLength(32),

                        DatePicker::make('date_of_birth')
                            ->label('Birthdate')
                            ->prefixIcon('heroicon-o-calendar-days')
                            ->displayFormat('M d, Y')
                            ->native(false)
                            ->weekStartsOnSunday()
                            ->minDate(now()->subYears(100))
                            ->maxDate(now())
                            ->closeOnDateSelection()
                            ->required(),
                    ]),

                Group::make()
                    ->schema([
                        Section::make('Credentials')
                            ->icon('heroicon-o-identification')
                            ->schema([
                                TextInput::make('email')
                                    ->prefixIcon('heroicon-o-envelope')
                                    ->disabled()
                                    ->columnSpanFull(),

                                TextInput::make('username')
                                    ->prefixIcon('heroicon-o-user')
                                    ->maxLength(128)
                                    ->columnSpanFull()
                                    ->required(),

                                $this->getPasswordFormComponent()
                                    ->prefixIcon('heroicon-o-lock-closed')
                                    ->columnSpanFull(),
                                $this->getPasswordConfirmationFormComponent()
                                    ->prefixIcon('heroicon-o-lock-closed')
                                    ->columnSpanFull(),
                            ])
                    ]),

                Group::make()
                    ->relationship('profile')
                    ->columns(2)
                    ->schema([
                        Section::make('Home Address')
                            ->relationship('homeaddress')
                            ->icon('heroicon-o-map-pin')
                            ->compact()
                            ->columns(2)
                            ->schema([
                                TextInput::make('street_name')
                                    ->columnSpanFull()
                                    ->autofocus(),
                                TextInput::make('street_extended'),
                                TextInput::make('city'),
                                // The following select is filled with whatever is selected on Country select
                                Select::make('state')
                                    ->options(
                                        function (Get $get) {
                                            return ($get('country') == '')
                                                ? []
                                                : ListBuilder::where(
                                                    'parent_id',
                                                    ListBuilder::whereNull('parent_id')->whereSlug($get('country'))->first()->id
                                                )->pluck('name', 'slug');
                                        }
                                    ),
                                TextInput::make('postal_code'),
                                // The following select fills the State select
                                Select::make('country')
                                    ->options(
                                        ListBuilder::where('parent_id', 35)
                                            ->pluck('name', 'slug')
                                    )
                                    ->live()
                                    ->columnSpanFull(),
                            ]),

                        Section::make('Home Phone')
                            ->icon('heroicon-o-phone')
                            ->relationship('primaryphone')
                            ->compact()
                            ->columns(1)
                            ->schema([
                                TextInput::make('number')
                                    ->label(false)
                                    ->tel(),
                            ]),

                        Section::make('Cell Phone')
                            ->icon('heroicon-o-device-phone-mobile')
                            ->relationship('secondaryphone')
                            ->compact()
                            ->columns(1)
                            ->schema([
                                TextInput::make('number')
                                    ->label(false)
                                    ->tel(),
                            ]),
                    ]),
            ]);
    }
}
