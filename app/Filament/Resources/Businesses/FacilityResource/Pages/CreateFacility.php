<?php

namespace App\Filament\Resources\Businesses\FacilityResource\Pages;

use App\Filament\Resources\Businesses\FacilityResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFacility extends CreateRecord
{
    protected static string $resource = FacilityResource::class;
}
