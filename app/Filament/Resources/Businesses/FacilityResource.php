<?php

namespace App\Filament\Resources\Businesses;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Get;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use App\Models\Businesses\Facility;
use App\Models\Settings\ListBuilder;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Section;
use Filament\Support\Enums\FontWeight;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Actions\ActionGroup;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\Businesses\FacilityResource\Pages;
use App\Filament\Resources\Businesses\FacilityResource\RelationManagers;

class FacilityResource extends Resource
{
    protected static ?string $model = Facility::class;

    protected static ?string $navigationIcon = 'heroicon-o-building-office-2';

    protected static ?string $modelLabel = 'Facilities';

    protected static ?string $pluralModelLabel = 'Facilities';

    protected static ?string $slug = 'facilities';

    protected static ?string $navigationGroup = 'Settings';

    protected static ?string $navigationLabel = 'Facilities';

    protected static ?int $navigationSort = 1;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Group::make()
                    ->columns(2)
                    ->columnSpanFull()
                    ->schema([

                        Group::make()
                            ->schema([
                                Section::make('Main Details')
                                    ->compact()
                                    ->icon('heroicon-o-clipboard-document-list')
                                    ->schema([
                                        Select::make('filing_as')
                                            ->options([
                                                1 => 'Group',
                                                2 => 'Individual'
                                            ])
                                            ->columnSpanFull()
                                            ->native(false)
                                            ->live()
                                            ->required(),
                                        Group::make()
                                            ->hidden(fn (Get $get): bool => ($get('filing_as') != 1))
                                            ->schema([
                                                TextInput::make('group_name')
                                                    ->label('Group Name')
                                                    ->maxLength(128)
                                                    ->columnSpanFull(),
                                            ]),
                                        Group::make()
                                            ->columns(3)
                                            ->hidden(fn (Get $get): bool => ($get('filing_as') != 2))
                                            ->schema([
                                                TextInput::make('last_name')
                                                    ->label('Last Name')
                                                    ->maxLength(64),
                                                TextInput::make('first_name')
                                                    ->label('First Name')
                                                    ->maxLength(64),
                                                TextInput::make('middle_name')
                                                    ->label('Middle Name')
                                                    ->maxLength(32),
                                            ]),
                                    ]),

                                Section::make('Contact Details')
                                    ->compact()
                                    ->collapsed()
                                    ->columns(2)
                                    ->icon('heroicon-o-phone')
                                    ->schema([
                                        Group::make()
                                            ->relationship('phone_contact')
                                            ->schema([
                                                TextInput::make('number')
                                                    ->label('Phone')
                                                    ->placeholder('+0 000 000-0000')
                                                    ->required()
                                                    ->tel()
                                                    ->maxLength(24),
                                            ]),
                                        Group::make()
                                            ->relationship('fax_contact')
                                            ->schema([
                                                TextInput::make('number')
                                                    ->label('Fax')
                                                    ->placeholder('+0 000 000-0000')
                                                    ->tel()
                                                    ->maxLength(24),
                                            ]),
                                        TextInput::make('email_address')
                                            ->label('E-mail')
                                            ->email()
                                            ->maxLength(64),
                                        TextInput::make('website_url')
                                            ->label('Website')
                                            ->url()
                                            ->maxLength(128),
                                    ]),

                                Section::make('Address')
                                    ->relationship('address')
                                    ->compact()
                                    ->collapsed()
                                    ->columns(2)
                                    ->icon('heroicon-o-map-pin')
                                    ->schema([
                                        TextInput::make('street_name')
                                            ->columnSpanFull()
                                            ->required()
                                            ->maxLength(128),
                                        TextInput::make('street_extended')
                                            ->maxLength(128),
                                        TextInput::make('city')
                                            ->required()
                                            ->maxLength(64),
                                        // The following select fills the State select
                                        Select::make('country')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('countries')
                                                        ->first()
                                                        ->id
                                                )->get()->pluck('name', 'slug')
                                            )
                                            ->native(false)
                                            ->columnSpanFull()
                                            ->live(),
                                        // The following select is filled with whatever is selected on Country select
                                        Select::make('state')
                                            ->options(
                                                function (Get $get) {
                                                    return ($get('country') == '')
                                                        ? []
                                                        : ListBuilder::whereParentId(
                                                            ListBuilder::whereNull('parent_id')
                                                                ->whereSlug($get('country'))
                                                                ->first()
                                                                ->id
                                                        )->get()->pluck('name', 'slug');
                                                }
                                            )
                                            ->native(false)
                                            ->searchable(),
                                        TextInput::make('postal_code')
                                            ->maxLength(12),
                                    ]),

                                Section::make('Pay-to Address')
                                    ->relationship('pay_to_address')
                                    ->collapsed()
                                    ->compact()
                                    ->columns(2)
                                    ->icon('heroicon-o-credit-card')
                                    ->schema([
                                        TextInput::make('street_name')
                                            ->columnSpanFull()
                                            ->required()
                                            ->maxLength(128),
                                        TextInput::make('street_extended')
                                            ->maxLength(128),
                                        TextInput::make('city')
                                            ->required()
                                            ->maxLength(64),
                                        // The following select fills the State select
                                        Select::make('country')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('countries')
                                                        ->first()
                                                        ->id
                                                )->get()->pluck('name', 'slug')
                                            )
                                            ->native(false)
                                            ->columnSpanFull()
                                            ->live(),
                                        // The following select is filled with whatever is selected on Country select
                                        Select::make('state')
                                            ->options(
                                                function (Get $get) {
                                                    return ($get('country') == '')
                                                        ? []
                                                        : ListBuilder::whereParentId(
                                                            ListBuilder::whereNull('parent_id')
                                                                ->whereSlug($get('country'))
                                                                ->first()
                                                                ->id
                                                        )->get()->pluck('name', 'slug');
                                                }
                                            )
                                            ->native(false)
                                            ->searchable(),
                                        TextInput::make('postal_code')
                                            ->maxLength(12),
                                    ]),

                                Section::make('Credit Cards Available')
                                    ->compact()
                                    ->collapsed()
                                    ->columns(2)
                                    ->icon('heroicon-o-credit-card')
                                    ->schema([
                                        Toggle::make('cc_payment_mastercard')
                                            ->label('Mastercard'),
                                        Toggle::make('cc_payment_visa')
                                            ->label('Visa'),
                                        Toggle::make('cc_payment_discover')
                                            ->label('Discover'),
                                        Toggle::make('cc_payment_amex')
                                            ->label('Amex'),
                                    ]),
                            ]),

                        Group::make()
                            ->schema([
                                Section::make('Business Details')
                                    ->compact()
                                    ->columns(2)
                                    ->icon('heroicon-o-clipboard-document-list')
                                    ->schema([
                                        TextInput::make('facility_npi')
                                            ->label('NPI')
                                            ->maxLength(12)
                                            ->required(),
                                        TextInput::make('clia_number')
                                            ->label('CLIA #')
                                            ->maxLength(12),
                                        TextInput::make('practice_id')
                                            ->label('Practice Identifier')
                                            ->maxLength(4)
                                            ->required(),
                                        TextInput::make('billing_attn')
                                            ->label('Attention Text')
                                            ->maxLength(64),
                                    ]),

                                Section::make('Place of Service')
                                    ->compact()
                                    ->columns(4)
                                    ->icon('heroicon-o-building-office')
                                    ->schema([
                                        Select::make('place_of_service_code')
                                            ->options(
                                                function (Get $get) {
                                                    return ($get('place_of_service') == '')
                                                        ? []
                                                        : ListBuilder::whereParentId(
                                                            ListBuilder::whereNull('parent_id')
                                                                ->whereSlug('place-of-service')
                                                                ->first()
                                                                ->id
                                                        )->whereValue($get('place_of_service'))
                                                        ->get()
                                                        ->pluck('value', 'value');
                                                }
                                            )
                                            ->label('POS Code')
                                            ->disabled()
                                            ->native(true),
                                        Select::make('place_of_service')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('place-of-service')
                                                        ->first()
                                                        ->id
                                                )->get()->pluck('combined_name_value', 'value')
                                            )
                                            ->label('Place of Service name')
                                            ->columnSpan(3)
                                            ->optionsLimit(200)
                                            ->native(false)
                                            ->live()
                                            ->searchable(),
                                    ]),

                                Section::make('Settings')
                                    ->compact()
                                    ->columns(2)
                                    ->icon('heroicon-o-cog-8-tooth')
                                    ->schema([
                                        Toggle::make('billing_location'),
                                        Toggle::make('primary_business_entity'),
                                        Toggle::make('service_location'),
                                        Toggle::make('accepts_assignment'),
                                        Toggle::make('reference_lab'),
                                    ]),
                            ])
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('full_name')
                    ->label('Facility')
                    ->searchable(['group_name', 'first_name', 'middle_name', 'last_name'])
                    ->sortable(['group_name', 'first_name', 'middle_name', 'last_name'])
                    ->weight(FontWeight::Bold),
                TextColumn::make('address.full')
                    ->label('Address')
                    ->searchable(['street_name', 'city', 'state', 'postal_code'])
                    ->sortable(['street_name'])
                    ->html(),
                TextColumn::make('phone_contact.number')
                    ->label('Phone')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('type')
                    ->label('Type')
                    ->html(),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                ActionGroup::make([
                    Tables\Actions\EditAction::make(),
                ])
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ])
            ->defaultSort('billing_location', 'desc')
            ->emptyStateIcon('heroicon-o-building-office-2')
            ->emptyStateHeading('No Facilities available.')
            ->emptyStateDescription('Once a facility is created in the system, it will appear here.');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFacilities::route('/'),
            'create' => Pages\CreateFacility::route('/create'),
            'edit' => Pages\EditFacility::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
