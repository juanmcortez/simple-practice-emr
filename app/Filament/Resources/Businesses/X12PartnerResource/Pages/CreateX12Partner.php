<?php

namespace App\Filament\Resources\Businesses\X12PartnerResource\Pages;

use App\Filament\Resources\Businesses\X12PartnerResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateX12Partner extends CreateRecord
{
    protected static string $resource = X12PartnerResource::class;
}
