<?php

namespace App\Filament\Resources\Businesses\X12PartnerResource\Pages;

use App\Filament\Resources\Businesses\X12PartnerResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditX12Partner extends EditRecord
{
    protected static string $resource = X12PartnerResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }
}
