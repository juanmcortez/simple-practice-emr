<?php

namespace App\Filament\Resources\Businesses\SpecialistResource\Pages;

use App\Filament\Resources\Businesses\SpecialistResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSpecialist extends CreateRecord
{
    protected static string $resource = SpecialistResource::class;
}
