<?php

namespace App\Filament\Resources\Businesses;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Get;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use App\Models\Settings\ListBuilder;
use Filament\Forms\Components\Group;
use App\Models\Businesses\Specialist;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Section;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Actions\ActionGroup;
use Filament\Forms\Components\RichEditor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\Businesses\SpecialistResource\Pages;
use App\Filament\Resources\Businesses\SpecialistResource\RelationManagers;

class SpecialistResource extends Resource
{
    protected static ?string $model = Specialist::class;

    protected static ?string $navigationIcon = 'heroicon-o-user-group';

    protected static ?string $modelLabel = 'Specialists';

    protected static ?string $pluralModelLabel = 'Specialists';

    protected static ?string $slug = 'specialists';

    protected static ?string $navigationGroup = 'Settings';

    protected static ?string $navigationLabel = 'Specialists';

    protected static ?int $navigationSort = 2;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Group::make()
                    ->columns(2)
                    ->columnSpanFull()
                    ->schema([

                        Group::make()
                            ->schema([
                                Section::make('Main Details')
                                    ->compact()
                                    ->icon('heroicon-o-clipboard-document-list')
                                    ->columns(2)
                                    ->schema([
                                        Select::make('title')
                                            ->native(false)
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('title')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'slug')
                                            ),
                                        TextInput::make('last_name')
                                            ->label('Last name')
                                            ->maxLength(64)
                                            ->required(),
                                        TextInput::make('first_name')
                                            ->label('First name')
                                            ->maxLength(64)
                                            ->required(),
                                        TextInput::make('middle_name')
                                            ->label('Middle name')
                                            ->maxLength(32),

                                        Select::make('type')
                                            ->native(false)
                                            ->required()
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('specialist-type')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'value')
                                            ),
                                        TextInput::make('specialty')
                                            ->label('Specialty')
                                            ->maxLength(64),
                                        TextInput::make('organization')
                                            ->label('Organization')
                                            ->maxLength(64),
                                        TextInput::make('valedictory')
                                            ->label('Valedictory')
                                            ->maxLength(64),

                                        TextInput::make('npi')
                                            ->label('NPI #')
                                            ->maxLength(12)
                                            ->required(),
                                        TextInput::make('taxonomy')
                                            ->label('Taxonomy #')
                                            ->maxLength(12),
                                        TextInput::make('upin')
                                            ->label('UPIN #')
                                            ->maxLength(12),
                                        TextInput::make('tin')
                                            ->label('TIN #')
                                            ->maxLength(12),
                                    ]),
                            ]),

                        Group::make()
                            ->schema([

                                Section::make('Contact Details')
                                    ->compact()
                                    ->collapsible()
                                    ->columns(2)
                                    ->icon('heroicon-o-phone')
                                    ->schema([
                                        Group::make()
                                            ->relationship('home_phone')
                                            ->schema([
                                                TextInput::make('number')
                                                    ->label('Home phone')
                                                    ->placeholder('+0 000 000-0000')
                                                    ->nullable()
                                                    ->tel(),
                                            ]),
                                        Group::make()
                                            ->relationship('cell_phone')
                                            ->schema([
                                                TextInput::make('number')
                                                    ->label('Cellphone')
                                                    ->placeholder('+0 000 000-0000')
                                                    ->nullable()
                                                    ->tel()
                                                    ->maxLength(24),
                                            ]),
                                        Group::make()
                                            ->relationship('work_phone')
                                            ->schema([
                                                TextInput::make('number')
                                                    ->label('Work phone')
                                                    ->required()
                                                    ->placeholder('+0 000 000-0000')
                                                    ->tel()
                                                    ->maxLength(24),
                                            ]),
                                        Group::make()
                                            ->relationship('fax')
                                            ->schema([
                                                TextInput::make('number')
                                                    ->label('Fax')
                                                    ->placeholder('+0 000 000-0000')
                                                    ->nullable()
                                                    ->tel()
                                                    ->maxLength(24),
                                            ]),
                                        Group::make()
                                            ->relationship('assistant_phone')
                                            ->columnSpanFull()
                                            ->columns(2)
                                            ->schema([
                                                TextInput::make('number')
                                                    ->label('Assistant phone')
                                                    ->requiredIf('work_phone.number', null)
                                                    ->placeholder('+0 000 000-0000')
                                                    ->nullable()
                                                    ->tel()
                                                    ->maxLength(24),
                                            ]),
                                        TextInput::make('email_address')
                                            ->label('E-mail')
                                            ->email()
                                            ->maxLength(64),
                                        TextInput::make('website_url')
                                            ->label('Website')
                                            ->url()
                                            ->maxLength(128),
                                    ]),

                                Section::make('Address')
                                    ->relationship('address')
                                    ->compact()
                                    ->collapsed()
                                    ->columns(2)
                                    ->icon('heroicon-o-map-pin')
                                    ->schema([
                                        TextInput::make('street_name')
                                            ->columnSpanFull()
                                            ->maxLength(128),
                                        TextInput::make('street_extended')
                                            ->maxLength(128),
                                        TextInput::make('city')
                                            ->maxLength(64),
                                        // The following select fills the State select
                                        Select::make('country')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('countries')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'slug')
                                            )
                                            ->native(false)
                                            ->columnSpanFull()
                                            ->live(),
                                        // The following select is filled with whatever is selected on Country select
                                        Select::make('state')
                                            ->options(
                                                function (Get $get) {
                                                    return ($get('country') == '')
                                                        ? []
                                                        : ListBuilder::whereParentId(
                                                            ListBuilder::whereNull('parent_id')
                                                                ->whereSlug($get('country'))
                                                                ->first()
                                                                ->id
                                                        )->pluck('name', 'slug');
                                                }
                                            )
                                            ->native(false)
                                            ->searchable(),
                                        TextInput::make('postal_code')
                                            ->maxLength(12),
                                    ]),

                                Section::make('Alternative address')
                                    ->relationship('alternative_address')
                                    ->compact()
                                    ->collapsed()
                                    ->columns(2)
                                    ->icon('heroicon-o-map-pin')
                                    ->schema([
                                        TextInput::make('street_name')
                                            ->maxLength(128)
                                            ->columnSpanFull(),
                                        TextInput::make('street_extended')
                                            ->maxLength(128),
                                        TextInput::make('city')
                                            ->maxLength(64),
                                        // The following select fills the State select
                                        Select::make('country')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('countries')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'slug')
                                            )
                                            ->native(false)
                                            ->columnSpanFull()
                                            ->live(),
                                        // The following select is filled with whatever is selected on Country select
                                        Select::make('state')
                                            ->options(
                                                function (Get $get) {
                                                    return ($get('country') == '')
                                                        ? []
                                                        : ListBuilder::whereParentId(
                                                            ListBuilder::whereNull('parent_id')
                                                                ->whereSlug($get('country'))
                                                                ->first()
                                                                ->id
                                                        )->pluck('name', 'slug');
                                                }
                                            )
                                            ->native(false)
                                            ->searchable(),
                                        TextInput::make('postal_code')
                                            ->maxLength(12),
                                    ]),

                            ]),

                        Group::make()
                            ->columnSpanFull()
                            ->schema([

                                Section::make('Specialist notes')
                                    ->compact()
                                    ->schema([
                                        RichEditor::make('notes')
                                            ->label(false)
                                            ->toolbarButtons([
                                                'bold',
                                                'italic',
                                                'link',
                                                'strike',
                                                'underline',
                                                'link',
                                                'bulletList',
                                                'orderedList',
                                                'undo',
                                                'redo',
                                            ]),
                                    ]),
                            ]),

                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                /*TextColumn::make('organization')
                    ->searchable()
                    ->sortable(),*/
                TextColumn::make('full_name')
                    ->label('Name')
                    ->searchable(['first_name', 'middle_name', 'last_name'])
                    ->sortable(['first_name', 'middle_name', 'last_name']),
                TextColumn::make('npi')
                    ->label('NPI')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('type_name')
                    ->label('Type')
                    ->searchable(['type'])
                    ->sortable(['type']),
                TextColumn::make('specialty')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('work_phone.number')
                    ->label('Phone')
                    ->searchable()
                    ->sortable(),
                /*TextColumn::make('fax.number')
                    ->searchable()
                    ->sortable(),*/
                TextColumn::make('address.full')
                    ->label('Address')
                    ->searchable(['street_name', 'city', 'state', 'postal_code'])
                    ->sortable(['street_name'])
                    ->html(),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                ActionGroup::make([
                    Tables\Actions\EditAction::make(),
                ]),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSpecialists::route('/'),
            'create' => Pages\CreateSpecialist::route('/create'),
            'edit' => Pages\EditSpecialist::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
