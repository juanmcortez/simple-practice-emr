<?php

namespace App\Filament\Resources\Businesses;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use App\Models\Settings\ListBuilder;
use Filament\Forms\Components\Group;
use App\Models\Businesses\X12Partner;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Section;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Actions\ActionGroup;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\Businesses\X12PartnerResource\Pages;
use App\Filament\Resources\Businesses\X12PartnerResource\RelationManagers;

class X12PartnerResource extends Resource
{
    protected static ?string $model = X12Partner::class;

    protected static ?string $navigationIcon = 'heroicon-o-inbox-stack';

    protected static ?string $modelLabel = 'X12 Partners';

    protected static ?string $pluralModelLabel = 'X12 Partners';

    protected static ?string $slug = 'x12-partners';

    protected static ?string $navigationGroup = 'Settings';

    protected static ?string $navigationLabel = 'X12 Partners';

    protected static ?int $navigationSort = 3;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Group::make()
                    ->columns(2)
                    ->columnSpanFull()
                    ->schema([

                        Group::make()
                            ->schema([
                                Section::make('Main Details')
                                    ->compact()
                                    ->icon('heroicon-o-clipboard-document-list')
                                    ->columns(2)
                                    ->schema([
                                        TextInput::make('name')
                                            ->label('Partner name')
                                            ->maxLength(64)
                                            ->required(),
                                        TextInput::make('etin')
                                            ->label('ID # (ETIN)')
                                            ->maxLength(16),
                                        Select::make('version')
                                            ->native(false)
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('x12-version')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'slug')
                                            ),
                                        Select::make('processing_format')
                                            ->native(false)
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('x12-processing-format')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'slug')
                                            ),
                                    ]),
                                Section::make('Login Details')
                                    ->compact()
                                    ->icon('heroicon-o-arrow-left-end-on-rectangle')
                                    ->columns(2)
                                    ->schema([
                                        TextInput::make('isa01')
                                            ->label('User login indicator ~ Use 00 or 03')
                                            ->prefix('ISA01')
                                            ->default('00')
                                            ->maxLength(64)
                                            ->required(),
                                        TextInput::make('isa03')
                                            ->label('User pass. indicator ~ Use 00 or 03')
                                            ->prefix('ISA03')
                                            ->default('00')
                                            ->maxLength(64)
                                            ->required(),
                                        TextInput::make('isa02')
                                            ->label('User ~ If 00 above leave spaces')
                                            ->prefix('ISA02')
                                            ->default('          ')
                                            ->maxLength(64)
                                            ->requiredIf('isa01', '03'),
                                        TextInput::make('isa04')
                                            ->label('Password ~ If 00 above leave spaces')
                                            ->prefix('ISA04')
                                            ->default('          ')
                                            ->maxLength(64)
                                            ->requiredIf('isa03', '03'),
                                    ]),
                            ]),

                        Group::make()
                            ->schema([
                                Section::make('Sender / Receiver Details')
                                    ->compact()
                                    ->icon('heroicon-o-arrows-right-left')
                                    ->schema([
                                        Select::make('isa05')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('x12-qualifier')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'value')
                                            )
                                            ->label('Sender ID qualifier')
                                            ->prefix('ISA05')
                                            ->native(false),
                                        TextInput::make('isa06')
                                            ->label('Sender ID')
                                            ->prefix('ISA06')
                                            ->maxLength(64),
                                        Select::make('isa07')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('x12-qualifier')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'value')
                                            )
                                            ->label('Receiver ID qualifier')
                                            ->prefix('ISA07')
                                            ->native(false),
                                        TextInput::make('isa08')
                                            ->label('Receiver ID')
                                            ->prefix('ISA08')
                                            ->maxLength(64),
                                    ]),
                            ]),

                        Group::make()
                            ->columnSpanFull()
                            ->schema([
                                Section::make('Application Details')
                                    ->compact()
                                    ->icon('heroicon-o-cog-8-tooth')
                                    ->columns(5)
                                    ->schema([
                                        Select::make('isa14')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('x12-isa-14')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'value')
                                            )
                                            ->label('Acknowledgment Requested')
                                            ->default(true)
                                            ->native(false),
                                        Select::make('isa15')
                                            ->options(
                                                ListBuilder::whereParentId(
                                                    ListBuilder::whereNull('parent_id')
                                                        ->whereSlug('x12-isa-15')
                                                        ->first()
                                                        ->id
                                                )->pluck('name', 'value')
                                            )
                                            ->label('Usage Indicator')
                                            ->default('T')
                                            ->native(false),
                                        TextInput::make('gs02')
                                            ->label('Application Sender Code')
                                            ->maxLength(16),
                                        TextInput::make('gs03')
                                            ->label('Application Receiver Code')
                                            ->maxLength(16),
                                        TextInput::make('per06')
                                            ->label('Submitter EDI Access #')
                                            ->maxLength(16),
                                    ]),
                            ])

                    ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('isa06')
                    ->label('Sender ID')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('isa08')
                    ->label('Receiver ID')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('version_value')
                    ->label('Version')
                    ->searchable()
                    ->sortable(),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                ActionGroup::make([
                    Tables\Actions\EditAction::make(),
                ]),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListX12Partners::route('/'),
            'create' => Pages\CreateX12Partner::route('/create'),
            'edit' => Pages\EditX12Partner::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
