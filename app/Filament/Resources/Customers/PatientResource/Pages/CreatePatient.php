<?php

namespace App\Filament\Resources\Customers\PatientResource\Pages;

use App\Filament\Resources\Customers\PatientResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreatePatient extends CreateRecord
{
    protected static string $resource = PatientResource::class;
}
