<?php

namespace App\Filament\Resources\Customers;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Get;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use App\Models\Customers\Patient;
use Filament\Tables\Actions\Action;
use App\Models\Settings\ListBuilder;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Select;
use Filament\Support\Enums\Alignment;
use Filament\Forms\Components\Section;
use Filament\Support\Enums\FontWeight;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Model;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Actions\ActionGroup;
use Filament\Forms\Components\DatePicker;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\Customers\PatientResource\Pages;
use App\Filament\Resources\Customers\PatientResource\RelationManagers;

class PatientResource extends Resource
{
    protected static ?string $model = Patient::class;

    protected static ?string $navigationIcon = 'heroicon-o-users';

    protected static ?string $navigationGroup = 'Patients';

    protected static ?string $navigationLabel = 'List';

    protected static ?int $navigationSort = 0;

    public static function getNavigationBadge(): ?string
    {
        return static::getModel()::count();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Group::make()
                    ->schema([
                        Section::make()
                            ->columns(2)
                            ->compact()
                            ->schema([
                                TextInput::make('pid')
                                    ->label('PID')
                                    ->disabled(),
                                TextInput::make('external_id')
                                    ->label('External ID'),
                            ]),
                    ]),

                Group::make()
                    ->relationship('profile')
                    ->columns(2)
                    ->schema([
                        Section::make('Patient Information')
                            ->columns(2)
                            ->compact()
                            ->schema([
                                TextInput::make('last_name')
                                    ->required()
                                    ->autofocus()
                                    ->maxLength(64),
                                TextInput::make('first_name')
                                    ->required()
                                    ->maxLength(64),
                                TextInput::make('middle_name')
                                    ->maxLength(32),

                                DatePicker::make('date_of_birth')
                                    ->label('Birthdate')
                                    ->prefixIcon('heroicon-o-calendar-days')
                                    ->displayFormat('M d, Y')
                                    ->native(false)
                                    ->weekStartsOnSunday()
                                    ->minDate(now()->subYears(100))
                                    ->maxDate(now())
                                    ->closeOnDateSelection()
                                    ->required(),
                            ]),

                        Section::make('Home Address')
                            ->relationship('homeaddress')
                            ->icon('heroicon-o-map-pin')
                            ->compact()
                            ->columns(2)
                            ->schema([
                                TextInput::make('street_name')
                                    ->columnSpanFull()
                                    ->autofocus(),
                                TextInput::make('street_extended'),
                                TextInput::make('city'),
                                // The following select is filled with whatever is selected on Country select
                                Select::make('state')
                                    ->options(
                                        function (Get $get) {
                                            return ($get('country') == '')
                                                ? []
                                                : ListBuilder::where(
                                                    'parent_id',
                                                    ListBuilder::whereNull('parent_id')->whereSlug($get('country'))->first()->id
                                                )->pluck('name', 'slug');
                                        }
                                    ),
                                TextInput::make('postal_code'),
                                // The following select fills the State select
                                Select::make('country')
                                    ->options(
                                        ListBuilder::where('parent_id', 35)
                                            ->pluck('name', 'slug')
                                    )
                                    ->live()
                                    ->columnSpanFull(),
                            ]),

                        Section::make('Home Phone')
                            ->icon('heroicon-o-phone')
                            ->relationship('primaryphone')
                            ->compact()
                            ->columns(1)
                            ->schema([
                                TextInput::make('number')
                                    ->label(false)
                                    ->tel(),
                            ]),

                        Section::make('Cell Phone')
                            ->icon('heroicon-o-device-phone-mobile')
                            ->relationship('secondaryphone')
                            ->compact()
                            ->columns(1)
                            ->schema([
                                TextInput::make('number')
                                    ->label(false)
                                    ->tel(),
                            ]),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('profile.full_name')
                    ->label('Full name')
                    ->searchable(['first_name', 'middle_name', 'last_name'])
                    ->sortable(['first_name', 'middle_name', 'last_name'])
                    ->weight(FontWeight::Bold),
                TextColumn::make('profile.date_of_birth')
                    ->label('Birthdate')
                    ->date('M d, Y')
                    ->searchable()
                    ->sortable()
                    ->toggleable()
                    ->alignment(Alignment::Center)->grow(false),
                TextColumn::make('profile.primaryphone.number')
                    ->label('Home phone')
                    ->alignment(Alignment::Center)
                    ->searchable()
                    ->sortable()
                    ->toggleable(),
                TextColumn::make('pid')
                    ->label('PID')
                    ->tooltip('Patient ID')
                    ->searchable()
                    ->sortable()
                    ->alignment(Alignment::Center),
                TextColumn::make('external_id')
                    ->label('EID')
                    ->tooltip('External ID')
                    ->searchable()
                    ->sortable()
                    ->alignment(Alignment::Center),
                TextColumn::make('created_at')
                    ->label('Available since')
                    ->color('primary')
                    ->tooltip(fn (Model $record): string => "{$record->created_at->format('M d, Y')}")
                    ->since()
                    ->toggleable()
                    ->alignment(Alignment::Center),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                ActionGroup::make([
                    Tables\Actions\ViewAction::make(),
                    Tables\Actions\EditAction::make(),
                    //Tables\Actions\DeleteAction::make(),
                ]),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ])
            ->defaultSort('profile.full_name')
            ->emptyStateIcon('heroicon-o-users')
            ->emptyStateHeading('No patients available.')
            ->emptyStateDescription('Once a patient is created in the system, it will appear here.');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPatients::route('/'),
            'create' => Pages\CreatePatient::route('/create'),
            'edit' => Pages\EditPatient::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
