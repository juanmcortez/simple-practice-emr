<?php

namespace App\Filament\Resources\Settings\ListBuilderResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use App\Filament\Resources\Settings\ListBuilderResource;

class ListListBuilders extends ListRecords
{
    protected static string $resource = ListBuilderResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
