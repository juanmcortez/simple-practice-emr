<?php

namespace App\Filament\Resources\Settings\ListBuilderResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use App\Filament\Resources\Settings\ListBuilderResource;

class EditListBuilder extends EditRecord
{
    protected static string $resource = ListBuilderResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }
}
