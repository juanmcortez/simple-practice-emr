<?php

namespace App\Filament\Resources\Settings;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Set;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Illuminate\Support\Str;
use Filament\Resources\Resource;
use App\Models\Settings\ListBuilder;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Section;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Actions\ActionGroup;
use Filament\Forms\Components\RichEditor;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Filament\Tables\Filters\TernaryFilter;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\Settings\ListBuilderResource\Pages;
use App\Filament\Resources\Settings\ListBuilderResource\RelationManagers;

class ListBuilderResource extends Resource
{
    protected static ?string $model = ListBuilder::class;

    protected static ?string $navigationIcon = 'heroicon-o-cog-8-tooth';

    protected static ?string $modelLabel = 'Items Creator';

    protected static ?string $pluralModelLabel = 'Items Creator';

    protected static ?string $slug = 'items-creator';

    protected static ?string $navigationGroup = 'Settings';

    protected static ?string $navigationLabel = 'Items Creator';

    protected static ?int $navigationSort = 99;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make()
                    ->compact()
                    ->columns(5)
                    ->schema([
                        Select::make('parent_id')
                            ->relationship(
                                name: 'parent',
                                titleAttribute: 'name',
                                modifyQueryUsing: fn (Builder $query) => $query->distinct()->whereNull('parent_id'),
                            )
                            ->label(false)
                            ->columnSpanFull(),
                        TextInput::make('name')
                            ->label(false)
                            ->placeholder('Item name')
                            ->columnSpan(2)
                            ->live(onBlur: true)
                            ->unique(ListBuilder::class, 'name', ignoreRecord: true)
                            ->afterStateUpdated(function (string $operation, $state, Set $set) {
                                if ($operation !== 'create') {
                                    return;
                                }
                                $set('slug', Str::slug($state));
                            })
                            ->required(),
                        TextInput::make('slug')
                            ->label(false)
                            ->placeholder('Item slug')
                            ->columnSpan(2)
                            ->disabled()
                            ->dehydrated()
                            ->unique(ListBuilder::class, 'slug', ignoreRecord: true)
                            ->required(),
                        Toggle::make('is_visible')
                            ->label('Active?')
                            ->default(true),
                        RichEditor::make('description')
                            ->toolbarButtons([
                                'bold',
                                'italic',
                                'link',
                                'strike',
                                'underline',
                                'link',
                                'bulletList',
                                'orderedList',
                                'undo',
                                'redo',
                            ])
                            ->columnSpanFull(),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                    ->searchable()
                    ->sortable()
                    ->toggleable(),
                TextColumn::make('parent.name')->label('Parent')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('value')
                    ->toggleable(),
                TextColumn::make('order')
                    ->sortable()
                    ->toggleable(),
                TextColumn::make('description')
                    ->wrap()
                    ->searchable()
                    ->sortable()
                    ->toggleable(),
                IconColumn::make('is_visible')->label('Active?')->boolean(),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make()
                    ->label('Deleted Items'),

                TernaryFilter::make('is_visible')
                    ->label('Active Status')
                    ->boolean()
                    ->trueLabel('Display active items.')
                    ->falseLabel('Display inactive items.')
                    ->native(true),

                SelectFilter::make('parent_id')
                    ->label('Items Parents')
                    ->relationship(
                        name: 'parent',
                        titleAttribute: 'name',
                        modifyQueryUsing: fn (Builder $query) => $query->distinct()->whereNull('parent_id'),
                    )
                    ->default(41)
            ])
            ->actions([
                ActionGroup::make([
                    Tables\Actions\ViewAction::make(),
                    Tables\Actions\EditAction::make(),
                    Tables\Actions\DeleteAction::make(),
                ]),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListListBuilders::route('/'),
            'create' => Pages\CreateListBuilder::route('/create'),
            'edit' => Pages\EditListBuilder::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
