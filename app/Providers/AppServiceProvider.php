<?php

namespace App\Providers;

use Filament\Tables\Table;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // --- All the tables default config ---
        Table::configureUsing(function (Table $table): void {
            $table
                ->paginationPageOptions([50, 100, 250, 500, 'all'])
                ->deferLoading()
                ->striped()
                ->persistSearchInSession()
                ->persistColumnSearchesInSession()
                ->persistSortInSession();
        });
        // --- All the tables default config ---
    }
}
