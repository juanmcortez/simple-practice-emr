<?php

namespace App\Models\Commons;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Phone extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'type',
        'number',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
    ];


    /**
     * Interact with the phone's number.
     */
    protected function number(): Attribute
    {
        return Attribute::make(
            get: function (?string $value) {
                if ($value) {
                    $phoneLenght = Str::length($value);
                    switch ($phoneLenght) {
                        case 14:
                            $intCodLenght = $areaCodStart = 4;
                            break;
                        case 13:
                            $intCodLenght = $areaCodStart = 3;
                            break;
                        default:
                            $intCodLenght = $areaCodStart = 2;
                            break;
                    }
                    $intCode = Str::substr($value, 0, $intCodLenght);
                    $areCode = Str::substr($value, $areaCodStart, 3);
                    $phoCode = Str::substr($value, ($phoneLenght - 7), 3);
                    $numCode = Str::substr($value, ($phoneLenght - 4), 4);
                    return $intCode . ' ' . $areCode . ' ' . $phoCode . '-' . $numCode;
                } else {
                    return null;
                }
            },
            set: function (?string $value) {
                if ($value) {
                    $frstRemov = Str::replace(')', '', $value);
                    $scndRemov = Str::replace('(', '', $frstRemov);
                    $thrdRemov = Str::replace('-', '', $scndRemov);
                    return Str::replace(' ', '', $thrdRemov);
                } else {
                    return null;
                }
            }
        );
    }
}
