<?php

namespace App\Models\Commons;

use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Address extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'street_name',
        'street_extended',
        'city',
        'state',
        'postal_code',
        'country',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full'
    ];


    /**
     * Get the full formatted address.
     */
    protected function full(): Attribute
    {
        if ($this->state) {
            $state = ListBuilder::whereSlug($this->state)->first()->name;
            return new Attribute(
                get: fn () => "{$this->street_name},<br />{$this->city},<br />{$state} - {$this->postal_code}",
            );
        } else {
            return new Attribute(
                get: fn () => null,
            );
        }
    }
}
