<?php

namespace App\Models\Commons;

use App\Models\Commons\Phone;
use App\Models\Commons\Address;
use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Profile extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'date_of_birth',
        'address_id',
        'primaryphone_id',
        'secondaryphone_id',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'address_id',
        'primaryphone_id',
        'secondaryphone_id',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'date_of_birth'     => 'date:M d, Y',
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name'];


    /**
     * Get the patient's full name.
     */
    protected function fullName(): Attribute
    {
        return new Attribute(
            get: fn () => "{$this->last_name}, {$this->first_name} {$this->middle_name}",
        );
    }


    /**
     * Home address relationship
     *
     * @return HasOne
     */
    public function homeaddress(): HasOne
    {
        return $this->hasOne(Address::class, 'id', 'address_id')->withDefault();
    }


    /**
     * Home phone relationship
     *
     * @return HasOne
     */
    public function primaryphone(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'primaryphone_id')
            ->whereType(ListBuilder::whereName('Homephone')->first()->slug)
            ->withDefault();
    }


    /**
     * Cell phone relationship
     *
     * @return HasOne
     */
    public function secondaryphone(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'secondaryphone_id')
            ->whereType(ListBuilder::whereName('Cellphone')->first()->slug)
            ->withDefault();
    }
}
