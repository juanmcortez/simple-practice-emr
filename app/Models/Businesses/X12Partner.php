<?php

namespace App\Models\Businesses;

use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class X12Partner extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'etin',
        'version',
        'processing_format',
        'isa01',
        'isa02',
        'isa03',
        'isa04',
        'isa05',
        'isa06',
        'isa07',
        'isa08',
        'isa14',
        'isa15',
        'gs02',
        'gs03',
        'per06',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'isa14' => 'boolean'
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'version_value',
    ];


    /**
     * Get the partner version.
     */
    protected function versionValue(): Attribute
    {
        return new Attribute(
            get: fn () =>
            ListBuilder::whereParentId(
                ListBuilder::whereNull('parent_id')->whereSlug('x12-version')->first()->id
            )->whereValue($this->version)
                ->first()
                ->value,
        );
    }
}
