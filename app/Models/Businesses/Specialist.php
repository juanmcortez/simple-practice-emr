<?php

namespace App\Models\Businesses;

use App\Models\Commons\Phone;
use App\Models\Commons\Address;
use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Specialist extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'first_name',
        'middle_name',
        'last_name',
        'npi',
        'upin',
        'tin',
        'taxonomy',
        'type',
        'specialty',
        'organization',
        'valedictory',
        'address_id',
        'alt_address_id',
        'homephone_id',
        'cellphone_id',
        'workphone_id',
        'fax_id',
        'assistant_number_id',
        'website_url',
        'email_address',
        'notes',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'type_name',
    ];


    /**
     * Get the faciliy full name.
     */
    protected function fullName(): Attribute
    {
        return new Attribute(
            get: fn () => "{$this->last_name}, {$this->first_name} {$this->middle_name}",
        );
    }


    /**
     * Get the faciliy full type.
     */
    protected function typeName(): Attribute
    {
        return new Attribute(
            get: fn () => ListBuilder::whereValue($this->type)->first()->name,
        );
    }


    /**
     * Address relationship
     *
     * @return HasOne
     */
    public function address(): HasOne
    {
        return $this->hasOne(Address::class, 'id', 'address_id')->withDefault();
    }


    /**
     * Address relationship
     *
     * @return HasOne
     */
    public function alternative_address(): HasOne
    {
        return $this->hasOne(Address::class, 'id', 'alt_address_id')->withDefault();
    }


    /**
     * Home phone relationship
     *
     * @return HasOne
     */
    public function home_phone(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'homephone_id')
            ->whereType(ListBuilder::whereName('Homephone')->first()->slug)
            ->withDefault();
    }


    /**
     * Cellphone relationship
     *
     * @return HasOne
     */
    public function cell_phone(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'cellphone_id')
            ->whereType(ListBuilder::whereName('Cellphone')->first()->slug)
            ->withDefault();
    }


    /**
     * Work phone relationship
     *
     * @return HasOne
     */
    public function work_phone(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'workphone_id')
            ->whereType(ListBuilder::whereName('Work phone')->first()->slug)
            ->withDefault();
    }


    /**
     * Fax relationship
     *
     * @return HasOne
     */
    public function fax(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'fax_id')
            ->whereType(ListBuilder::whereName('Fax number')->first()->slug)
            ->withDefault();
    }


    /**
     * Assistant phone relationship
     *
     * @return HasOne
     */
    public function assistant_phone(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'assistant_number_id')
            ->whereType(ListBuilder::whereName('Assistant phone')->first()->slug)
            ->withDefault();
    }
}
