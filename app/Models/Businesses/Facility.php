<?php

namespace App\Models\Businesses;

use Illuminate\Support\Str;
use App\Models\Commons\Phone;
use App\Models\Commons\Address;
use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Facility extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'filing_as',
        'group_name',
        'first_name',
        'middle_name',
        'last_name',
        'facility_npi',
        'practice_id',
        'billing_attn',
        'clia_number',
        'facility_taxonomy',
        'billing_location',
        'service_location',
        'primary_business_entity',
        'accepts_assignment',
        'reference_lab',
        'place_of_service_code',
        'place_of_service',
        'cc_payment_mastercard',
        'cc_payment_visa',
        'cc_payment_discover',
        'cc_payment_amex',
        'address_id',
        'pay_to_address_id',
        'phone_id',
        'fax_id',
        'website_url',
        'email_address',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'billing_location'          => 'boolean',
        'service_location'          => 'boolean',
        'primary_business_entity'   => 'boolean',
        'accepts_assignment'        => 'boolean',
        'reference_lab'             => 'boolean',
        'cc_payment_mastercard'     => 'boolean',
        'cc_payment_visa'           => 'boolean',
        'cc_payment_discover'       => 'boolean',
        'cc_payment_amex'           => 'boolean',
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'type'
    ];


    /**
     * Get the faciliy full name.
     */
    protected function fullName(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->filing_as == 2) ? "{$this->last_name}, {$this->first_name} {$this->middle_name}" : "{$this->group_name}",
        );
    }


    /**
     * Get the faciliy full type.
     */
    protected function type(): Attribute
    {
        $facilityType  = ($this->billing_location) ? '<strong>Billing Location</strong><br />' : null;
        $facilityType .= ($this->service_location) ? 'Service Location<br />' : null;
        $facilityType .= ($this->primary_business_entity) ? 'Pri. Business Entity<br />' : null;
        $facilityType .= ($this->reference_lab) ? 'Reference Lab' : null;
        $facilityType = Str::replace('<br /><br />', '', $facilityType);
        $facilityType = Str::replaceEnd('<br />', '', $facilityType);
        return new Attribute(
            get: fn () => $facilityType,
        );
    }


    /**
     * Address relationship
     *
     * @return HasOne
     */
    public function address(): HasOne
    {
        return $this->hasOne(Address::class, 'id', 'address_id')->withDefault();
    }


    /**
     * Pay To Address relationship
     *
     * @return HasOne
     */
    public function pay_to_address(): HasOne
    {
        return $this->hasOne(Address::class, 'id', 'pay_to_address_id')->withDefault();
    }


    /**
     * Home phone relationship
     *
     * @return HasOne
     */
    public function phone_contact(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'phone_id')
            ->whereType(ListBuilder::whereName('Homephone')->first()->slug)
            ->withDefault();
    }


    /**
     * Fax relationship
     *
     * @return HasOne
     */
    public function fax_contact(): HasOne
    {
        return $this->hasOne(Phone::class, 'id', 'fax_id')
            ->whereType(ListBuilder::whereName('Fax number')->first()->slug)
            ->withDefault();
    }
}
