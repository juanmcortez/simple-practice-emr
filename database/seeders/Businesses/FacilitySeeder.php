<?php

namespace Database\Seeders\Businesses;

use Illuminate\Database\Seeder;
use App\Models\Businesses\Facility;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Total number of fake facilities
        $max = fake()->numberBetween(1, 6);

        // Create them
        Facility::factory($max)->create();
    }
}
