<?php

namespace Database\Seeders\Businesses;

use Illuminate\Database\Seeder;
use App\Models\Businesses\Specialist;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SpecialistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Total number of fake facilities
        $max = fake()->numberBetween(1, 12);

        // Create them
        Specialist::factory($max)->create();
    }
}
