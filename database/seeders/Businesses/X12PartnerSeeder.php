<?php

namespace Database\Seeders\Businesses;

use Illuminate\Database\Seeder;
use App\Models\Businesses\X12Partner;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class X12PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        X12Partner::factory()->createOne();
    }
}
