<?php

namespace Database\Seeders\Settings;

use Illuminate\Support\Str;
use Hamcrest\Type\IsInteger;
use Hamcrest\Type\IsNumeric;
use Illuminate\Database\Seeder;
use App\Models\Settings\ListBuilder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ListBuilderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $itemsList = [
            'Specialist Type'       => ['Distributor', 'Imaging Service', 'Immunization Service', 'Lab Service', 'Other', 'Referring Physician', 'Specialist', 'Vendor'],
            'Argentina'             => ['B' => 'Buenos Aires', 'K' => 'Catamarca', 'H' => 'Chaco', 'U' => 'Chubut', 'C' => 'Ciudad Autónoma de Buenos Aires', 'W' => 'Corrientes', 'X' => 'Córdoba', 'E' => 'Entre Ríos', 'P' => 'Formosa', 'Y' => 'Jujuy', 'L' => 'La Pampa', 'F' => 'La Rioja', 'M' => 'Mendoza', 'N' => 'Misiones', 'Q' => 'Neuquén ', 'R' => 'Río Negro', 'A' => 'Salta', 'J' => 'San Juan', 'D' => 'San Luis', 'Z' => 'Santa Cruz', 'S' => 'Santa Fe', 'G' => 'Santiago del Estero', 'V' => 'Tierra del Fuego', 'T' => 'Tucumán'],

            'Countries'             => ['AR' => 'Argentina', 'US' => 'United States'],
            'Ethnicity'             => ['Hispanic or Latino', 'Not Hispanic or Latino'],
            'Financial Class'       => ['Self Pay', 'Medicaid', 'Medicaid MCO', 'Commercial', 'Blue Shield', 'Medicare', 'Workers Comp'],
            'Genders'               => ['Ambiguous', 'Female', 'Male', 'Other', 'Unknown'],
            'Language'              => ['Armenian', 'Chinese', 'Danish', 'Deaf', 'English', 'Farsi', 'French', 'German', 'Greek', 'Hmong', 'Italian', 'Japanese', 'Korean', 'Laotian', 'Mien', 'Norwegian', 'Others', 'Portuguese', 'Punjabi', 'Russian', 'Spanish', 'Tagalog', 'Turkish', 'Vietnamese', 'Yiddish', 'Zulu'],

            'Marital Status'        => ['Divorced', 'Domestic Partner', 'Married', 'Separated', 'Single', 'Widowed'],
            'Note Type'             => ['Bill/Collect', 'Chart Note', 'Insurance', 'Lab Results', 'New Document', 'New Orders', 'Other', 'Patient Reminders', 'Pharmacy', 'Prior Auth', 'Referral', 'Test Scheduling', 'Unassigned'],

            'Payment Codes'         => ['AETPAY', 'BCPAY', 'CIGPAY', 'COPAY', 'HUMPAY', 'INSPAY', 'MAPPAY', 'MCDPAY', 'MCOPAY', 'MCRPAY', 'MVAPAY', 'PATPAY', 'SLFPAY', 'TRIPAY', 'UHCPAY', 'WCPAY'],

            'Payment Dates'         => ['Date', 'Deposit Date', 'Post To Date'],
            'Payment Payer'         => ['Pat', 'Ins1', 'Ins2', 'Ins3'],
            'Payment Method'        => ['Authorize.net', 'Bank Draft', 'Cash', 'Check Payment', 'Credit Card', 'Electronic'],
            'Payment Status'        => ['Fully Paid', 'Unapplied'],
            'Payment Type'          => ['Patient', 'Insurance'],
            'Phone Types'           => ['Homephone', 'Cellphone', 'Fax number', 'Work phone', 'Emergency phone', 'Assistant phone'],
            'Price Level'           => ['Standard'],
            'Race'                  => ['American Indian or Alaska Native', 'Asian', 'Black or African American', 'Native Hawaiian or Other Pacific Islander', 'White'],

            'Referring Source'      => ['Coupon', 'Direct Mail', 'Employee', 'Newspaper', 'Other', 'Patient', 'Radio', 'Referral Card', 'T.V.', 'Walk-In'],

            'Risks Factors'         => ['Allergies', 'Asthma', 'Breast Disease', 'Contact Lenses', 'Contraceptive Complication (specify)', 'Diabetes', 'Depression', 'Epilepsy', 'Fibroids', 'Gall Bladder Condition', 'Heart Disease', 'Hepatitis', 'Hypertension', 'Infertility', 'Severe Migraine', 'Other (specify)', 'PID (Pelvic Inflammatory Disease)', 'Sickle Cell', 'Thrombosis/Stroke', 'Varicose Veins'],

            'Risk Level'            => ['High', 'Low', 'Medium'],
            'Statement Suppression' => ['Suppress Statements', 'Don\'t Suppress Statements'],
            'Subscriber Relation'   => ['Child', 'Other', 'Self', 'Spouse'],
            'Title'                 => ['Dr.', 'Mr.', 'Mrs.', 'Ms.'],
            'United States'         => ['AL' => 'Alabama', 'AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas', 'CA' => 'California', 'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware', 'DC' => 'District of Columbia', 'FL' => 'Florida', 'GA' => 'Georgia', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois', 'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana', 'ME' => 'Maine', 'MD' => 'Maryland', 'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota', 'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana', 'NE' => 'Nebraska', 'NV' => 'Nevada', 'NH' => 'New Hampshire', 'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York', 'NC' => 'North Carolina', 'ND' => 'North Dakota', 'OH' => 'Ohio', 'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania', 'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota', 'TN' => 'Tennessee', 'TX' => 'Texas', 'UT' => 'Utah', 'VT' => 'Vermont', 'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia', 'WI' => 'Wisconsin', 'WY' => 'Wyoming', 'AS' => 'American Samoa', 'GU' => 'Guam', 'MP' => 'Northern Mariana Islands', 'PR' => 'Puerto Rico', 'UM' => 'United States Minor Outlying Islands', 'VI' => 'Virgin Islands'],

            'Place of Service'      => ['Unassigned', 'Telehealth Provided Other than in Patient\'s Home', 'School', 'Homeless Shelter', 'Indian Health Service Free-standing Facility', 'Indian Health Service Provider-based Facility', 'Tribal 638 Free-standing Facility', 'Tribal 638 Provider-based Facility', 'Unassigned', 'Telehealth Provided in Patient\'s Home', 'Office ', 'Home', 'Assisted Living Facility', 'Group Home *', 'Mobile Unit', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Urgent Care Facility', 'Inpatient Hospital', 'Outpatient Hospital', 'Emergency Room - Hospital', 'Ambulatory Surgical Center', 'Birthing Center ', 'Military Treatment Facility', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Skilled Nursing Facility', 'Nursing Facility', 'Custodial Care Facility', 'Hospice', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Ambulance - Land', 'Ambulance - Air or Water', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Independent Clinic', 'Federally Qualified Health Center', 'Inpatient Psychiatric Facility', 'Psychiatric Facility-Partial Hospitalization', 'Community Mental Health Center', 'Intermediate Care Facility/Developmentally challenged', 'Residential Substance Abuse Treatment Facility', 'Psychiatric Residential Treatment Center', 'Non-residential Substance Abuse Treatment Facility', 'Unassigned', 'Unassigned', 'Mass Immunization Center', 'Comprehensive Inpatient Rehabilitation Facility', 'Comprehensive Outpatient Rehabilitation Facility', 'Unassigned', 'Unassigned', 'End-Stage Renal Disease Treatment Facility', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Public Health Clinic', 'Rural Health Clinic', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Independent Laboratory', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Unassigned', 'Other Place of Service'],

            'X12 ISA 14'            => ['No', 'Yes'],
            'X12 ISA 15'            => ['T' => 'Testing', 'P' => 'Production'],
            'X12 Processing Format' => ['standard', 'medi-cal', 'cms'],
            'X12 Qualifier'         => ['01' => 'Duns (Dun & Bradstreet)', '14' => 'Duns Plus Suffix', '20' => 'Health Industry Number (HIN)', '27' => 'Carrier ID from HCFA', '28' => 'Fiscal Intermediary ID from HCFA', '29' => 'Medicare ID from HCFA', '30' => 'U.S. Federal Tax ID Number', '33' => 'NAIC Company Code', 'ZZ' => 'Mutually Defined'],

            'X12 Version'           => ['005010X222A1', '004010X098A1'],
        ];

        $jdx = 1;
        foreach ($itemsList as $parent => $childrens) {
            $currentParent = ListBuilder::factory()->create([
                'parent_id'     => null,
                'name'          => $parent,
                'slug'          => Str::slug($parent),
                'value'         => null,
                'order'         => $jdx++,
                'description'   => null,
            ]);
            $order = 0;
            foreach ($childrens as $idx => $value) {
                if (is_integer($idx)) {
                    $order = $idx + 1;
                } else {
                    $order += 1;
                }

                $itemValues = [
                    'parent_id'     => $currentParent->id,
                    'name'          => $value,
                    'slug'          => Str::slug($value),
                    'order'         => $order,
                    'description'   => null,
                ];

                switch ($currentParent->slug) {
                    case 'place-of-service':
                        $itemValues['value'] = $order;
                        break;
                    case 'x12-isa-14':
                        $itemValues['value'] = $idx;
                        break;
                    case 'argentina':
                    case 'countries':
                    case 'united-states':
                    case 'x12-isa-15':
                    case 'x12-qualifier':
                        $itemValues['value'] = Str::upper($idx);
                        break;
                    case 'payment-codes':
                    case 'x12-version':
                        $itemValues['value'] = Str::upper($value);
                        break;
                    default:
                        $itemValues['value'] = Str::lower(Str::replace(' ', '', $value));
                        break;
                }

                ListBuilder::factory()->create($itemValues);
            }
        }
    }
}
