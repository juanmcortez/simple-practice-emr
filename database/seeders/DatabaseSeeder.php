<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\Settings\ListBuilder;
use Illuminate\Support\Facades\Hash;
use Database\Seeders\Customers\PatientSeeder;
use Database\Seeders\Businesses\FacilitySeeder;
use Database\Seeders\Settings\ListBuilderSeeder;
use Database\Seeders\Businesses\SpecialistSeeder;
use Database\Seeders\Businesses\X12PartnerSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Create the settigns lists
        $this->callOnce(ListBuilderSeeder::class);

        // Create the facilities
        $this->callOnce(FacilitySeeder::class);

        // Create the Specialists
        $this->callOnce(SpecialistSeeder::class);

        // Create X12 Partner
        $this->callOnce(X12PartnerSeeder::class);

        // Create the first user
        $user = User::factory()->createOne([
            'username'          => 'juanmcortez',
            'email'             => 'juanm.cortez@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('Taiga-Craso-77º-Buzo-Gafar-Tronco'),
            'remember_token'    => Str::random(10),
        ]);

        $user->profile->update([
            'first_name'        => 'Juan',
            'middle_name'       => 'Manuel',
            'last_name'         => 'Cortéz',
            'date_of_birth'     => '1980-04-08',
        ]);

        $user->profile->homeaddress->update([
            'street_name'       => 'Dr. Francisco Muñiz 481',
            'street_extended'   => 'Piso 7 Dpto E',
            'city'              => 'Córdoba Capital',
            'state'             => ListBuilder::whereName('Córdoba')->first()->slug,
            'postal_code'       => '5003',
            'country'           => ListBuilder::whereName('Argentina')->first()->slug,
        ]);

        $user->profile->primaryphone->update([
            'number'            => '+5493516708210',
        ]);

        $user->profile->secondaryphone->update([
            'number'            => '+5493516708210',
        ]);

        // Create some randorm patients.
        $this->callOnce(PatientSeeder::class);
    }
}
