<?php

namespace Database\Seeders\Customers;

use Illuminate\Database\Seeder;
use App\Models\Customers\Patient;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Create 50 patients!
        Patient::factory()->count(fake()->randomNumber(2, true))->create();
    }
}
