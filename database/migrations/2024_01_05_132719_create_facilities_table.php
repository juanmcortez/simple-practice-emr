<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('filing_as')->default(1);

            $table->string('group_name', 128)->nullable();
            $table->string('first_name', 64)->nullable();
            $table->string('middle_name', 32)->nullable();
            $table->string('last_name', 64)->nullable();

            $table->string('facility_npi', 12);
            $table->string('practice_id', 4)->nullable();

            $table->string('billing_attn', 64)->nullable();
            $table->string('clia_number', 12)->nullable();
            $table->string('facility_taxonomy', 12)->nullable();

            $table->boolean('billing_location')->default(true);
            $table->boolean('service_location')->default(true);
            $table->boolean('primary_business_entity')->default(false);
            $table->boolean('accepts_assignment')->default(false);
            $table->boolean('reference_lab')->default(false);

            $table->unsignedBigInteger('place_of_service_code')->default(81);
            $table->unsignedBigInteger('place_of_service')->default(81);

            $table->boolean('cc_payment_mastercard')->default(false);
            $table->boolean('cc_payment_visa')->default(false);
            $table->boolean('cc_payment_discover')->default(false);
            $table->boolean('cc_payment_amex')->default(false);

            $table->foreignId('address_id')->nullable()->constrained('addresses')->cascadeOnDelete();
            $table->foreignId('pay_to_address_id')->nullable()->constrained('addresses')->cascadeOnDelete();

            $table->foreignId('phone_id')->nullable()->constrained('phones')->cascadeOnDelete();
            $table->foreignId('fax_id')->nullable()->constrained('phones')->cascadeOnDelete();

            $table->string('website_url', 128)->nullable();
            $table->string('email_address', 64)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('facilities');
    }
};
