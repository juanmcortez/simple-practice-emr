<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();

            $table->string('first_name', 64);
            $table->string('middle_name', 32)->nullable();
            $table->string('last_name', 64);

            $table->date('date_of_birth')->nullable();

            $table->foreignId('address_id')->nullable()->constrained()->cascadeOnDelete();
            $table->foreignId('primaryphone_id')->nullable()->constrained('phones')->cascadeOnDelete();
            $table->foreignId('secondaryphone_id')->nullable()->constrained('phones')->cascadeOnDelete();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Schema::dropIfExists('profiles');
    }
};
