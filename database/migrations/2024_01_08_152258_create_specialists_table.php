<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('specialists', function (Blueprint $table) {
            $table->id();

            $table->string('title')->nullable();
            $table->string('first_name', 64);
            $table->string('middle_name', 32)->nullable();
            $table->string('last_name', 64);

            $table->string('npi', 12);
            $table->string('upin', 12)->nullable();
            $table->string('tin', 12)->nullable();
            $table->string('taxonomy', 12)->nullable();

            $table->string('type')->nullable();
            $table->string('specialty', 64)->nullable();
            $table->string('organization', 64)->nullable();
            $table->string('valedictory', 64)->nullable();

            $table->foreignId('address_id')->nullable()->constrained('addresses')->cascadeOnDelete();
            $table->foreignId('alt_address_id')->nullable()->constrained('addresses')->cascadeOnDelete();

            $table->foreignId('homephone_id')->nullable()->constrained('phones')->cascadeOnDelete();
            $table->foreignId('cellphone_id')->nullable()->constrained('phones')->cascadeOnDelete();
            $table->foreignId('workphone_id')->nullable()->constrained('phones')->cascadeOnDelete();
            $table->foreignId('fax_id')->nullable()->constrained('phones')->cascadeOnDelete();
            $table->foreignId('assistant_number_id')->nullable()->constrained('phones')->cascadeOnDelete();

            $table->string('website_url', 128)->nullable();
            $table->string('email_address', 64)->nullable();

            $table->longText('notes')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('specialists');
    }
};
