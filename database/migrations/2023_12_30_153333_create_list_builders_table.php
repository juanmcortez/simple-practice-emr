<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('list_builders', function (Blueprint $table) {
            $table->id();

            $table->foreignId('parent_id')->nullable()->constrained('list_builders')->cascadeOnDelete();

            $table->string('name', 128);
            $table->string('slug', 128);

            $table->string('value', 128)->nullable();
            $table->integer('order')->default(1);

            $table->longText('description')->nullable();
            $table->boolean('is_visible')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('list_builders');
    }
};
