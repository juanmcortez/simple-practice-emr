<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('x12_partners', function (Blueprint $table) {
            $table->id();

            $table->string('name', 64);
            $table->string('etin', 16)->nullable();

            $table->string('version', 16)->nullable();
            $table->string('processing_format', 64)->nullable();

            $table->string('isa01', 16)->nullable()->default('00');
            $table->string('isa02', 16)->nullable()->default('          ');
            $table->string('isa03', 16)->nullable()->default('00');
            $table->string('isa04', 16)->nullable()->default('          ');

            $table->string('isa05', 16)->nullable();
            $table->string('isa06', 16)->nullable();
            $table->string('isa07', 16)->nullable();
            $table->string('isa08', 16)->nullable();

            $table->boolean('isa14')->default(true);
            $table->string('isa15', 1)->default('T');

            $table->string('gs02', 16)->nullable();
            $table->string('gs03', 16)->nullable();
            $table->string('per06', 16)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('x12_partners');
    }
};
