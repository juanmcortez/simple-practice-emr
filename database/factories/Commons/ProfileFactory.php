<?php

namespace Database\Factories\Commons;

use App\Models\Commons\Phone;
use App\Models\Commons\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Commons\Profile>
 */
class ProfileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = fake()->randomElement(['male', 'female']);
        return [
            'address_id'        => Address::factory()->createOne(),
            'primaryphone_id'   => Phone::factory()->createOne(['type' => 'homephone']),
            'secondaryphone_id' => Phone::factory()->createOne(['type' => 'cellphone']),
            'first_name'        => fake()->firstName($gender),
            'middle_name'       => fake()->firstName($gender),
            'last_name'         => fake()->lastName(),
            'date_of_birth'     => fake()->dateTimeBetween('-75 years', '-1 year')->format('Y-m-d'),

        ];
    }
}
