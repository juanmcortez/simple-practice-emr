<?php

namespace Database\Factories\Commons;

use Illuminate\Support\Str;
use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Commons\Address>
 */
class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $countriesID = ListBuilder::whereNull('parent_id')->whereName('Countries')->first()->id;
        $countryList = ListBuilder::whereParentId($countriesID)->get()->pluck('slug', 'name');
        $randCountry = fake()->randomElement($countryList);
        $selectCouID = ListBuilder::whereNull('parent_id')->whereSlug($randCountry)->first()->id;
        $listOfState = ListBuilder::whereParentId($selectCouID)->get()->pluck('slug');
        $fullstreet  = fake()->streetAddress();
        $streetname  = (Str::contains($fullstreet, 'Apt. '))
            ? Str::before($fullstreet, 'Apt. ')
            : (
                (Str::contains($fullstreet, 'Suite '))
                ? Str::before($fullstreet, 'Suite ')
                : $fullstreet
            );
        $streetExtra = (Str::contains($fullstreet, 'Apt. '))
            ? 'Apt. ' . Str::after($fullstreet, 'Apt. ')
            : (
                (Str::contains($fullstreet, 'Suite '))
                ? 'Suite ' . Str::after($fullstreet, 'Suite ')
                : null
            );
        return [
            'street_name'       => $streetname,
            'street_extended'   => $streetExtra,
            'city'              => fake()->city(),
            'state'             => fake()->randomElement($listOfState),
            'postal_code'       => fake()->postcode(),
            'country'           => $randCountry,
        ];
    }
}
