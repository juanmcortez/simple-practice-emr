<?php

namespace Database\Factories\Customers;

use App\Models\Commons\Profile;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customers\Patient>
 */
class PatientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $eid = fake()->randomLetter() . fake()->randomLetter() . fake()->randomLetter() . '-';
        $eid .= fake()->randomNumber(9, true);
        $eid = fake()->toUpper($eid);
        return [
            'external_id'   => fake()->randomElement([null, $eid]),
            'profile_id'    => Profile::factory()->create(),
            'created_at'    => fake()->dateTimeBetween('-2 years'),
        ];
    }
}
