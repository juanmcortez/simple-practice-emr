<?php

namespace Database\Factories\Settings;

use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Settings\ListBuilder>
 */
class ListBuilderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $listItem = fake()->toUpper(fake()->word());
        return [
            'parent_id'     => ListBuilder::class,
            'name'          => $listItem,
            'slug'          => fake()->toLower($listItem),
            'value'         => fake()->toLower($listItem),
            'description'   => fake()->paragraph(),
            'is_visible'    => true,
        ];
    }
}
