<?php

namespace Database\Factories\Businesses;

use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Businesses\X12Partner>
 */
class X12PartnerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $generic_code = fake()->toUpper(fake()->randomNumber(1, true) . fake()->randomLetter() . fake()->randomLetter() . fake()->randomLetter());
        $generic_receiver = fake()->randomNumber(9, true);

        return [
            'name'              => fake()->company(),
            'etin'              => fake()->randomNumber(9, true),
            'version'           => fake()->randomElement(ListBuilder::whereParentId(ListBuilder::whereNull('parent_id')->whereSlug('x12-version')->first()->id)->get()->pluck('value')),
            'processing_format' => fake()->randomElement(ListBuilder::whereParentId(ListBuilder::whereNull('parent_id')->whereSlug('x12-processing-format')->first()->id)->get()->pluck('value')),
            'isa01'             => '00',
            'isa02'             => '          ',
            'isa03'             => '00',
            'isa04'             => '          ',
            'isa05'             => fake()->randomElement(ListBuilder::whereParentId(ListBuilder::whereNull('parent_id')->whereSlug('x12-qualifier')->first()->id)->get()->pluck('value')),
            'isa06'             => $generic_code,
            'isa07'             => fake()->randomElement(ListBuilder::whereParentId(ListBuilder::whereNull('parent_id')->whereSlug('x12-qualifier')->first()->id)->get()->pluck('value')),
            'isa08'             => $generic_receiver . '000000',
            'isa14'             => true,
            'isa15'             => fake()->randomElement(ListBuilder::whereParentId(ListBuilder::whereNull('parent_id')->whereSlug('x12-isa-15')->first()->id)->get()->pluck('value')),
            'gs02'              => $generic_code,
            'gs03'              => $generic_receiver,
            'per06'             => fake()->randomNumber(5, true) . fake()->randomNumber(5, true),
        ];
    }
}
