<?php

namespace Database\Factories\Businesses;

use App\Models\Commons\Phone;
use App\Models\Commons\Address;
use App\Models\Settings\ListBuilder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Businesses\Specialist>
 */
class SpecialistFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender             = fake()->randomElement(['male', 'female']);
        $taxonomy           = fake()->randomNumber(3, true) .
            fake()->toUpper(fake()->randomLetter()) .
            fake()->randomNumber(5, true) .
            fake()->toUpper(fake()->randomLetter());
        $specialistsType    = ListBuilder::whereParentId(ListBuilder::whereNull('parent_id')->whereSlug('specialist-type')->first()->id)
            ->get()
            ->pluck('value');

        return [
            'title'                 => 'dr',
            'first_name'            => fake()->firstName($gender),
            'middle_name'           => fake()->firstName($gender),
            'last_name'             => fake()->lastName(),
            'npi'                   => fake()->randomNumber(5, true) . fake()->randomNumber(5, true),
            'upin'                  => null,
            'tin'                   => null,
            'taxonomy'              => $taxonomy,
            'type'                  => fake()->randomElement($specialistsType),
            'specialty'             => fake()->randomElement([null, fake()->jobTitle()]),
            'organization'          => fake()->randomElement([null, fake()->company()]),
            'valedictory'           => null,
            'address_id'            => Address::factory()->createOne(),
            'alt_address_id'        => fake()->randomElement([null, Address::factory()->createOne()]),
            'homephone_id'          => fake()->randomElement([null, Phone::factory()->createOne(['type' => 'homephone'])]),
            'cellphone_id'          => fake()->randomElement([null, Phone::factory()->createOne(['type' => 'cellphone'])]),
            'workphone_id'          => Phone::factory()->createOne(['type' => 'work-phone']),
            'fax_id'                => fake()->randomElement([null, Phone::factory()->createOne(['type' => 'fax-number'])]),
            'assistant_number_id'   => fake()->randomElement([null, Phone::factory()->createOne(['type' => 'assistant-phone'])]),
            'website_url'           => fake()->url(),
            'email_address'         => fake()->companyEmail(),
            'notes'                 => null,
        ];
    }
}
