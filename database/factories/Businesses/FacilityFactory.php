<?php

namespace Database\Factories\Businesses;

use App\Models\Commons\Phone;
use App\Models\Commons\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Businesses\Facility>
 */
class FacilityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $filling_as = fake()->randomElement([1, 2]);
        $gender     = fake()->randomElement(['male', 'female']);
        $clia       = fake()->randomNumber(2, true) . fake()->toUpper(fake()->randomLetter()) . fake()->randomNumber(7, true);
        $taxonomy   = fake()->randomNumber(3, true) . fake()->toUpper(fake()->randomLetter()) . fake()->randomNumber(5, true) . fake()->toUpper(fake()->randomLetter());
        $pos        = fake()->randomNumber(1, true);
        return [
            'filing_as'                 => $filling_as,
            'group_name'                => ($filling_as == 1) ? fake()->company() : null,
            'first_name'                => ($filling_as == 2) ? fake()->firstName($gender) : null,
            'middle_name'               => ($filling_as == 2) ? fake()->firstName($gender) : null,
            'last_name'                 => ($filling_as == 2) ? fake()->lastName() : null,
            'facility_npi'              => fake()->randomNumber(5, true) . fake()->randomNumber(5, true),
            'practice_id'               => fake()->toUpper(fake()->randomLetter() . fake()->randomLetter()),
            'billing_attn'              => null,
            'clia_number'               => $clia,
            'facility_taxonomy'         => $taxonomy,
            'billing_location'          => fake()->boolean(),
            'service_location'          => fake()->boolean(),
            'primary_business_entity'   => fake()->boolean(),
            'accepts_assignment'        => fake()->boolean(),
            'reference_lab'             => fake()->boolean(),
            'place_of_service_code'     => $pos,
            'place_of_service'          => $pos,
            'cc_payment_mastercard'     => fake()->boolean(),
            'cc_payment_visa'           => fake()->boolean(),
            'cc_payment_discover'       => fake()->boolean(),
            'cc_payment_amex'           => fake()->boolean(),
            'address_id'                => Address::factory()->createOne(),
            'pay_to_address_id'         => Address::factory()->createOne(),
            'phone_id'                  => Phone::factory()->createOne(['type' => 'homephone']),
            'fax_id'                    => Phone::factory()->createOne(['type' => 'fax-number']),
            'website_url'               => fake()->url(),
            'email_address'             => fake()->companyEmail(),
        ];
    }
}
